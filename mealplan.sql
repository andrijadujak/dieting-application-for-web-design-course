-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 10, 2021 at 07:34 AM
-- Server version: 10.4.16-MariaDB
-- PHP Version: 7.4.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `mealplan`
--
CREATE DATABASE IF NOT EXISTS `mealplan` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
USE `mealplan`;

-- --------------------------------------------------------

--
-- Table structure for table `kalendar`
--

CREATE TABLE `kalendar` (
  `Id` int(11) NOT NULL,
  `Date` varchar(50) NOT NULL,
  `RecipeId` int(11) DEFAULT NULL,
  `ClientId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `kalendar`
--

INSERT INTO `kalendar` (`Id`, `Date`, `RecipeId`, `ClientId`) VALUES
(88, '14/2', 6, 2);

-- --------------------------------------------------------

--
-- Table structure for table `korisnici`
--

CREATE TABLE `korisnici` (
  `Id` int(11) NOT NULL,
  `Username` varchar(20) NOT NULL,
  `Lozinka` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `korisnici`
--

INSERT INTO `korisnici` (`Id`, `Username`, `Lozinka`) VALUES
(2, 'andrija', '1234');

-- --------------------------------------------------------

--
-- Table structure for table `recepti`
--

CREATE TABLE `recepti` (
  `Id` int(11) NOT NULL,
  `Name` varchar(50) NOT NULL,
  `Calories` int(11) NOT NULL,
  `Fat` int(11) NOT NULL,
  `SaturatedFat` int(11) NOT NULL,
  `TransFat` int(11) NOT NULL,
  `Cholesterol` int(11) NOT NULL,
  `Salt` int(11) NOT NULL,
  `Carbohydrate` int(11) NOT NULL,
  `DiateryFiber` int(11) NOT NULL,
  `Sugar` int(11) NOT NULL,
  `Recipe` longtext NOT NULL,
  `Protein` smallint(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `recepti`
--

INSERT INTO `recepti` (`Id`, `Name`, `Calories`, `Fat`, `SaturatedFat`, `TransFat`, `Cholesterol`, `Salt`, `Carbohydrate`, `DiateryFiber`, `Sugar`, `Recipe`, `Protein`) VALUES
(1, 'jelo', 1, 1, 1, 1, 1, 1, 1, 1, 1, 'staviti jelo u pecnicu', 1),
(6, 'Homemade Chicken Noodle Soup', 273, 8, 1, 4, 58, 6, 26, 4, 0, '1 ½ tablespoons canola oil\r\n\r\n1 ½ cups thinly sliced carrot\r\n\r\n1 cup finely chopped onion\r\n\r\n⅔ cup thinly sliced celery\r\n\r\n2 cups water\r\n\r\n1 (32-ounce) container unsalted chicken stock (such as Swanson)\r\n\r\n1 teaspoon dried thyme or 3 fresh thyme sprigs\r\n\r\n6 ounces whole-grain rotini (such as Barilla; about 2 cups)\r\n\r\n8 ounces skinless, boneless rotisserie chicken breast, shredded\r\n\r\n4 ounces skinless, boneless rotisserie chicken thigh, shredded\r\n\r\n¾ teaspoon salt\r\n\r\n¼ teaspoon black pepper\r\n\r\nHeat a Dutch oven or large saucepan over medium-high heat. Add oil to pan; swirl to coat. Add carrot, onion, and celery; sauté 5 minutes.\r\n\r\nWhile vegetables cook, pour 2 cups water and stock into a microwave-safe bowl; microwave at HIGH for 5 minutes. (This saves up to 10 minutes in the pot.)\r\n\r\nAdd hot stock mixture to pan; bring to a boil. Stir in thyme and pasta; reduce heat to medium, and cook 8 minutes.\r\n\r\nAdd chicken, salt, and pepper to pan; cook 2 minutes or until thoroughly heated and pasta is tender.', 27),
(8, 'Garlic bread', 246, 8, 1, 4, 32, 2, 42, 2, 35, 'Put some garlic in some bread', 5);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `kalendar`
--
ALTER TABLE `kalendar`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `RecipeFK` (`RecipeId`),
  ADD KEY `ClientFK` (`ClientId`);

--
-- Indexes for table `korisnici`
--
ALTER TABLE `korisnici`
  ADD PRIMARY KEY (`Id`);

--
-- Indexes for table `recepti`
--
ALTER TABLE `recepti`
  ADD PRIMARY KEY (`Id`),
  ADD UNIQUE KEY `Name` (`Name`),
  ADD UNIQUE KEY `Name_2` (`Name`),
  ADD UNIQUE KEY `Name_3` (`Name`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `kalendar`
--
ALTER TABLE `kalendar`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=89;

--
-- AUTO_INCREMENT for table `korisnici`
--
ALTER TABLE `korisnici`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `recepti`
--
ALTER TABLE `recepti`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `kalendar`
--
ALTER TABLE `kalendar`
  ADD CONSTRAINT `ClientFK` FOREIGN KEY (`ClientId`) REFERENCES `korisnici` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `RecipeFK` FOREIGN KEY (`RecipeId`) REFERENCES `recepti` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
