<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>

  <style>
	.active, .tr:active{
		background-color: #666;
		color: white;
	}

	th{
		text-align: center;
	}

	.list-group{
		
	}
	.active, .tr:active:hover{
		background-color: #666;
		color: white;
	}

	.button{
		background-color: SeaGreen; 
		border: 2px solid CornflowerBlue;
		color: white;
		padding: 5px;
		text-align: center;
		text-decoration: none;
		display: inline-block;
		font-size: 16px;
		margin: 4px 2px;
		cursor: pointer;
		border-radius: 45%;
		opacity: 0.8;
		font-size: 18px;
	}	
	.logout{
	background-color:crimson;
	
	}

	#pweek{
		width: 100%;
		text-align: center;
		background-color: RoyalBlue;
		color:White;
		border-width:1px;
		padding-top:10px;
		padding-bottom:10px;
	}
	
	li{
		text-align:center;
	}
	
	#nweek{
		width: 100%;
		padding-top:10px;
		text-align: center;
		background-color:RoyalBlue;
		color:white;
		border-width:1px;
		padding-bottom:10px;
	}
  
	#weekly_meal_plan{
		width: 100%;
		margin-top:15px;
		margin-bottom:15px
	}
	#stranica{
		border-style:solid; 
		border-width:1px;
		border-radius: 5px;
		padding:10px;
		margin:10px;
		background-color:PapayaWhip;
	}
	#meal_plan_section{ 
		margin-left:15px;
	}
	#recipe{
		margin-left:15px;
	}
	#scroll{
	 height:850px;
	 overflow:auto;
	}
  </style>
</head>
<body onload="start();">

<div class="container-fluid">
  <div id="stranica">
    <div class="row">
      <div class="col-sm-4 container-fluid" style="display:inline-block">
		<section id="meal_plan_section">
			<label for="weekly_meal_plan">Weekly meal plan:</label><br>
			<button class="container-fluid" id="pweek" style="margin-bottom:4px;" onclick="remWeek();">Previous week</button>
			<ul class="list-group container-fluid" id="weekly_meal_plan">
				<li id="first" onclick="showTable(0);" style="text-align:center;"  class="list-group-item list-group-item-action"></li>
				<li id="second" onclick="showTable(1);" style="text-align:center;" class="list-group-item list-group-item-action"></li>
				<li id="third" onclick="showTable(2); " style="text-align:center;"  class="list-group-item list-group-item-action"></li>
				<li id="fourth" onclick="showTable(3); " style="text-align:center;"  class="list-group-item list-group-item-action"></li>
				<li id="fifth" onclick="showTable(4);" style="text-align:center;"  class="list-group-item list-group-item-action"></li>
				<li id="sixth" onclick="showTable(5);" style="text-align:center;"  class="list-group-item list-group-item-action"></li>
				<li id="seventh" onclick="showTable(6);" style="text-align:center;"  class="list-group-item list-group-item-action"></li>
			</ul>
			<button class="container-fluid" id="nweek" style="margin-top:4px;" onclick="addWeek();">Next week</button>

		</section><br>
		
		<section id="recipe">
		
		</section>
	  </div>
      <div class="col-sm-8">
		<div class="container-fluid" id="scroll">
			<?php
				require_once "spoj.php";
				define('DB_DATABASE', 'mealplan');
				$db = new mysqli(DB_SERVER,DB_USERNAME,DB_PASSWORD,DB_DATABASE);
				require_once "spoj.php";
				session_start();
				if (!$_SESSION['loggedin']){
					header("location: login.php");
				}else{ 
					$sql = "SELECT Name, Calories, Fat, SaturatedFat, TransFat, Protein, Cholesterol, Salt, Carbohydrate, DiateryFiber,Sugar FROM recepti";
					$result =$db -> query($sql);
					if ($result->num_rows > 0) {
					echo"<table id='tableOfRecipe' style='background-color:white;' class='table table-bordered table-hover'>
						<thead>
							<tr>
								<th>Name</th>
								<th>Calories per 100g</th>
								<th>Fat</th>
								<th>Saturated fat</th>
								<th>Trans fat</th>
								<th>Protein</th>
								<th>Cholesterol</th>
								<th>Salt</th>
								<th>Carbohydrate</th>
								<th>Dietery fiber</th>
								<th>Sugar</th>
							</tr>
						</thead>
						<tbody>";
						while($row = $result->fetch_assoc()){
							echo'<tr class="tr" id="'.$row["Name"].'" onclick=\'show("'.$row["Name"].'")\' >';
							echo'
							<td>'.$row["Name"].'</td>
							<td>'.$row["Calories"].'</td>
							<td>'.$row["Fat"].'</td>
							<td>'.$row["SaturatedFat"].'</td>
							<td>'.$row["TransFat"].'</td>
							<td>'.$row["Protein"].'</td>
							<td>'.$row["Cholesterol"].'</td>
							<td>'.$row["Salt"].'</td>
							<td>'.$row["Carbohydrate"].'</td>
							<td>'.$row["DiateryFiber"].'</td>
							<td>'.$row["Sugar"].'</td>
						</tr>';}
					echo'</tbody>
					</table>';}
				$db->close();}
				
					?>
				
				</div>
				<form action="dodajrecept.php">
					<input type="submit" style="margin-left:550px;" value="ADD RECIPE" />
				</form> <br>
				<aside style="float:left; margin-left:225px;" id="recipes_for_day">
				</aside>
	 <div>
		<button class="logout" style="float:right;"><p ><a style="color:white;" href="loggout.php">logout</a></p></button>
	 </div>	
	 </div>
    </div>
    <br>
  </div>
</div>

</body>
<script>
	const days = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"]
	const elemen = ["first", "second", "third", "fourth", "fifth", "sixth", "seventh"];
	var weeknumber=0;
	var weekday= new Date();
	
	function start(){
		setWeek();
		setActive();
	}
	
	function setActive(){
		var header=document.getElementById("scroll")
		var trs= header.getElementsByClassName("tr");
		for (var i = 0; i < trs.length; i++) {
			trs[i].addEventListener("click", function() {
				var current = document.getElementsByClassName("active");
				if (current.length > 0) { 
					current[0].className = current[0].className.replace(" active", "");
				}
				this.className += " active";
			});
		}
	}
	
	function setWeek() {
		  let d = new Date();
		  const start = d.getDate();
		  for (let i = 0; i < 7; i++) {
			d.setDate(start + i)
			document.getElementById(elemen[i]).innerHTML = days[d.getDay()]+"<button  class='add button' style='float:right; padding:10px; outline: none;' onclick='addRecipe("+i+");'>+</button>" + '<br>' + d.getDate() + "." + (d.getMonth() + 1);
		  }
	}
				
	function addWeek() {
	  weeknumber++;
	  for (let i = 0; i < 7; i++) {
		let d = new Date();
		const start = d.getDate();
		d.setDate(start + i + (weeknumber * 7));
		weekday.setDate(start + i + (weeknumber * 7) - 7);
		document.getElementById(elemen[i]).innerHTML = days[d.getDay()] +"<button  class='add button' style='float:right; padding:10px;' onclick='addRecipe("+i+");'>+</button>" + '<br>' + d.getDate() + "." + (d.getMonth() + 1);
		}
	}
	
	function remWeek(){
		if(weeknumber>0)
			weeknumber--;
			for(let i = 0;i < 7; i++){
				let d= new Date();
				const start =d.getDate();
				d.setDate(start + i + (weeknumber * 7));
				weekday.setDate(start + i + (weeknumber * 7) - 7);
				document.getElementById(elemen[i]).innerHTML = days[d.getDay()]+"<button  class='add button' style='float:right; padding:10px;' onclick='addRecipe("+i+");'>+</button>"  + '<br>' + d.getDate() + "." + (d.getMonth() + 1);
			}
	}
	
	function removeRecipe(a,b){
		var parent =  document.getElementById("recipes_for_day");
		var child = parent.firstChild;
		parent.removeChild(child);
		removeRecipeScript(a,b);
	}
	
	function removeRecipeScript(a,b){
		var xhttp = new XMLHttpRequest();
		xhttp.onreadystatechange = function() {
			if (this.readyState == 4 && this.status == 200){
				var div = document.createElement('div');
				div.innerHTML = this.responseText;
				document.getElementById("recipes_for_day").prepend(div); 
			}
		};
		xhttp.open("GET", "delrecipe.php?q="+a+"&w="+b, true);
		xhttp.send();
	}
	
	function showTable(a){
		var parent =  document.getElementById("recipes_for_day");
		var child = parent.firstChild;
		parent.removeChild(child);
		showTableScript(a);
	}
	

	
	function showTableScript(a){
		var xhttp = new XMLHttpRequest();
		let d = new Date();
		d.setDate(weekday.getDate()+a);
		
		var dateStr = d.getDate() + "/" + (d.getMonth() + 1);;
		xhttp.onreadystatechange = function() {
			if (this.readyState == 4 && this.status == 200){
				var div = document.createElement('div');
				div.innerHTML = this.responseText;
				document.getElementById("recipes_for_day").prepend(div); 
			}
		};
		xhttp.open("GET", "gettable.php?q="+dateStr, true);
		xhttp.send();
	}
	
		function show(a){
		var parent =  document.getElementById("recipe");
		var child = parent.firstChild;
		parent.removeChild(child);
		showRecipe(a);
	}
	
	function showRecipe(a){
		var xhttp = new XMLHttpRequest();
		xhttp.onreadystatechange = function() {
			if (this.readyState == 4 && this.status == 200){
			var div = document.createElement('div');
				div.innerHTML = this.responseText;
				document.getElementById("recipe").prepend(div); 
		}
	}
	xhttp.open("GET", "getrecipe.php?q="+a, true);
	xhttp.send();
}
	
	function addRecipe(a){
		var activetr = document.getElementsByClassName("tr active")[0];
		if(activetr){
			var parent =  document.getElementById("recipes_for_day");
			var child = parent.firstChild;
			parent.removeChild(child);
			addRecipeScript(a);
		}
	}
	
	function addRecipeScript(a){
		var xhttp = new XMLHttpRequest();
		let d = new Date();
		d.setDate(weekday.getDate()+a);
		console.log(weekday.getDate()+a);
		
		var dateStr = d.getDate() + "/" + (d.getMonth() + 1);;
		var activetr = document.getElementsByClassName("tr active")[0];
		var trid = activetr.getAttribute("id");
		var str="?q="+dateStr+"&w="+trid;
		xhttp.onreadystatechange = function() {
			if (this.readyState == 4 && this.status == 200){
				var div = document.createElement('div');
				div.innerHTML = this.responseText;
				document.getElementById("recipes_for_day").prepend(div); 
			}
		};
		
		xhttp.open("GET", "addrecipe.php"+str, true);
		xhttp.send();
	}

</script>
			
</html>