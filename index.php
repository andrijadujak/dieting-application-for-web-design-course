<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
  <script>
  var day= new Date();
  days = ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday","Sunday"];
  days[day.getDay();]
  var monday = new Date();
  var tuesday = new Date();
  var wednesday = new Date();
  var thursday = new Date();
  var friday = new Date();
  var saturday = new Date();
  var sunday = new Date();
  
  
  $(document).ready(function(){
	  $("submit").click(function(){
		$(login_section).hide();
		document.getElementById("meal_plan_section").style.display = "block";
		document.getElementById("recipe_section").style.display = "block";
		document.getElementById("nutrition_limit").style.display = "block";
	  });
	 });
  </script>
  <style>
	button{
		background-color: LawnGreen; 
		border: 1px solid CornflowerBlue;
		color: black;
		padding: 5px;
		text-align: center;
		text-decoration: none;
		display: inline-block;
		font-size: 16px;
		margin: 4px 2px;
		cursor: pointer;
		border-radius: 45%;
	}	
	};

	#weekly_meal_plan{
		margin-right:400px;
	}

	#login_section{
		background-color: LemonChiffon; 
		border-color:red;
		border-style:solid; 
		border-width:1px;
		padding:5px; 
		margin:15px;
		margin-right:375px;
	}

	#stranica{
		border-style:solid; 
		border-width:1px;
		border-radius: 5px;
		padding:10px;
		margin:10px;
		background-color:Lavender;
	}

	th{
		text-align: center;
	}

	#scroll{
	 height:1000px;
	 overflow:auto;
	}
  </style>
</head>
<body onload="setActive();">
<div class="container-fluid" on>
 
  <div class="container-fluid" id="stranica">
    <div class="row">
      <div class="col-sm-4">
		<section id="login_section">

				<button><a href="login.php">Login</a></button>
				<button  style="float:right;"><a href="register.php">Register</a></button>
		</section>

		<section id="recipe">
		
		</section>
	</div>
      <div class="col-sm-8">
		<div class="container" id="scroll">
			<?php
				require_once "spoj.php";
					define('DB_DATABASE', 'mealplan');
					$db = new mysqli(DB_SERVER,DB_USERNAME,DB_PASSWORD,DB_DATABASE);
					$sql = "SELECT Name, Calories, Fat, SaturatedFat, TransFat, Cholesterol, Salt, Carbohydrate, DiateryFiber, Sugar FROM recepti";
					$result =$db -> query($sql);
					if ($result->num_rows > 0) {
					echo"<table id='tableOfRecipe' style='background-color:white;' class='table table-bordered table-hover'>
						<thead>
							<tr>
								<th>Name</th>
								<th>Calories per 100g</th>
								<th>Fat</th>
								<th>Saturated fat</th>
								<th>Trans fat</th>
								<th>Cholesterol</th>
								<th>Salt</th>
								<th>Carbohydrate</th>
								<th>Diatery fiber</th>
								<th>Sugar</th>
							</tr>
						</thead>
						<tbody>";
						while($row = $result->fetch_assoc()){
							echo'
							<tr onclick=\'show("'.$row["Name"].'")\'>
							<td>'.$row["Name"].'</td>
							<td>'.$row["Calories"].'</td>
							<td>'.$row["Fat"].'</td>
							<td>'.$row["SaturatedFat"].'</td>
							<td>'.$row["TransFat"].'</td>
							<td>'.$row["Cholesterol"].'</td>
							<td>'.$row["Salt"].'</td>
							<td>'.$row["Carbohydrate"].'</td>
							<td>'.$row["DiateryFiber"].'</td>
							<td>'.$row["Sugar"].'</td>
						</tr>';}
					echo'</tbody>
					</table>';}
				$db->close();
			?>
		</div>	
	 </div>
    </div>
    <br>
  </div>
</div>
<script>

function setActive(){
		var header=document.getElementById("scroll")
		var trs= header.getElementsByClassName("tr");
		for (var i = 0; i < trs.length; i++) {
			trs[i].addEventListener("click", function() {
				var current = document.getElementsByClassName("active");
				if (current.length > 0) { 
					current[0].className = current[0].className.replace(" active", "");
				}
				this.className += " active";
			});
		}
	}

function show(a){
		var parent =  document.getElementById("recipe");
		var child = parent.firstChild;
		parent.removeChild(child);
		showRecipe(a);
	}

function showRecipe(a){
		var xhttp = new XMLHttpRequest();
		xhttp.onreadystatechange = function() {
			if (this.readyState == 4 && this.status == 200){
			var div = document.createElement('div');
				div.innerHTML = this.responseText;
				document.getElementById("recipe").prepend(div); 
		}
	}
	xhttp.open("GET", "getrecipe.php?q="+a, true);
	xhttp.send();
}

</script>
</body>
</html>


<?php


   define('DB_SERVER', 'localhost');
   define('DB_USERNAME', 'root');
   define('DB_PASSWORD', '');
   define('DB_DATABASE', 'mealplan');
   $db = mysqli_connect(DB_SERVER,DB_USERNAME,DB_PASSWORD,DB_DATABASE);
   if ($_SESSION['loggedin']){
		  header("location: loggedin.php");
   }



   session_start();
?>